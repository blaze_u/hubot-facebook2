{Response} = require 'hubot'

class FbResponse extends Response
  sendPrivate: (strings...) ->
    @robot.adapter.sendPrivate @envelope, strings...

  read: () ->
    @robot.adapter.read @envelope

  typing: () ->
    @robot.adapter.typing @envelope

module.exports = FbResponse
