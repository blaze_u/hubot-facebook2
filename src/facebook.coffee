{Robot, Adapter, TextMessage, EnterMessage, LeaveMessage, TopicMessage, Brain} = require 'hubot'

fs = require 'fs'
os = require 'os'
login = require 'facebook-chat-api'
FBResponse = require './response'

class Facebook extends Adapter

  send: (envelope, strings...) ->
    for str in strings
      @bot.sendMessage str, envelope.room, (err, info) =>
        @robot.logger.error err if err?

  sendPrivate: (envelope, strings...) ->
    @send room: envelope.user.id, strings...

  reply: (envelope, strings...) ->
    name = envelope.user.name.split(' ')[0]
    @send envelope, strings.map((str) -> "#{name}: #{str}")...

  topic: (envelope, strings...) ->
    title = strings.join(' ')
    thread = envelope.room
    @bot.setTitle title, thread

  typing: (envelope) ->
    @bot.sendTypingIndicator envelope.room, (err) ->
      @robot.logger.error err if err?

  read: (envelope) ->
    @bot.markAsRead envelope.room

  run: ->
    self = @
    stateDir = process.env.HUBOT_FB_STATEDIR || os.tmpdir()
    stateFile = "#{stateDir}/appstate.json"

    try
      appState = require(stateFile)
      @robot.logger.info "Found state file, will try to use that"
    catch error
      @robot.logger.warning "State file not found, will try to create on login"

    loginData =
      email: process.env.HUBOT_FB_EMAIL
      password: process.env.HUBOT_FB_PASSWORD
      appState: appState

    config =
      logLevel: 'error'
      listenEvents: true
      forceLogin: true

    # Override the response to provide custom method
    @robot.Response = FBResponse

    login loginData, config, (err, bot) ->
      return self.robot.logger.error err if err
      self.robot.logger.info "Connected as FBID: #{bot.getCurrentUserID()}"

      fs.writeFile stateFile, JSON.stringify(bot.getAppState()), (err) ->
        return self.robot.logger.error "Can't write state file #{err}" if err?
        self.robot.logger.info 'State file updated'

      self.bot = bot
      self.emit "connected"
      bot.listen (err, message) ->
        return self.robot.logger.error err if err

        senderID = +message.senderID or +message.author or +message.from or +message.reader
        if !message.senderName? and !self.robot.brain.data.users[senderID]
          return
        name = message.senderName or self.robot.brain.data.users[senderID].name
        user = self.robot.brain.userForId senderID, name: name, room: +message.threadID

        switch message.type
          when "message"
            self.receive new TextMessage user, message.body
          when "event"
            switch message.logMessageType
              when "log:thread-name"
                self.receive new TopicMessage user, message.logMessageData.name
              when "log:unsubscribe"
                self.receive new LeaveMessage user
              when "log:subscribe"
                self.receive new EnterMessage user

        self.robot.logger.debug "#{user.name} -> #{user.room}: #{message.body || message.logMessageBody || message.type}"

exports.use = (robot) ->
  new Facebook robot
